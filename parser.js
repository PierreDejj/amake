﻿const http = require('http');
const fs = require('fs');
const path = require('path');
const { send } = require('process');
const { dirname } = require('path');
const request = require("request-promise");
const cheerio = require("cheerio");
const mysql = require('mysql');
const mysql2 = require('mysql2');


const conn = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    database: "rpl",
    password: "password"
});

const connection = mysql2.createPool({
    host: "127.0.0.1",
    user: "root",
    password: "password",
    database: "rpl"
});



const scrapedData1 = [];
const scrapedData2 = [];
const scrapedData3 = [];
const scrapedData4 = [];
const scrapedData5 = [];
const scrapedData6 = [];
const scrapedData7 = [];
const scrapedData8 = [];
const scrapedData9 = [];
const scrapedData10 = [];
const scrapedData11 = [];
const scrapedData12 = [];
const scrapedData13 = [];
const scrapedData14 = [];
const scrapedData15 = [];
const scrapedData16 = [];
const scrapedData17 = [];
const scrapedData18 = [];
const scrapedData19 = [];
const scrapedData20 = [];

//Парсер команд
async function main() {
    const result = await request.get("https://www.sports.ru/rfpl/table/");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div > div.stat.mB6 > table > tbody > tr >td > div").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData1.push(name);

    });


}
//Парсер кол-во матчей
async function main1() {
    const result = await request.get("https://www.sports.ru/rfpl/table/");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = parseInt($(tds[2]).text());
        scrapedData2.push(name);
        //console.log(scrapedData2[0]);


    });
    // for (let j = 0; j < 16; j++) {
    // let query1 = "INSERT INTO rpl.rp  (`Mat`) VALUES ?";
    // console.log(scrapedData2);
    // conn.query(query1, [scrapedData2], (err, result) => {});
    // console.log(scrapedData2[j]);
    // }

}
//Парсер кол-во побед
async function main2() {
    const result = await request.get("https://www.sports.ru/rfpl/table/");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = parseInt($(tds[3]).text());
        scrapedData3.push(name);
        //console.log(scrapedData2[0]);


    });


}
//Парсер кол-во ничьих
async function main3() {
    const result = await request.get("https://www.sports.ru/rfpl/table/");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = parseInt($(tds[4]).text());
        scrapedData4.push(name);
        //console.log(scrapedData2[0]);


    });


}

//Парсер кол-во поражений
async function main4() {
    const result = await request.get("https://www.sports.ru/rfpl/table/");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = parseInt($(tds[5]).text());
        scrapedData5.push(name);
        //console.log(scrapedData2[0]);


    });


}

//Парсер кол-во забитых
async function main5() {
    const result = await request.get("https://www.sports.ru/rfpl/table/");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = parseInt($(tds[6]).text());
        scrapedData6.push(name);
        //console.log(scrapedData2[0]);


    });


}

//Парсер кол-во пропущенных
async function main6() {
    const result = await request.get("https://www.sports.ru/rfpl/table/");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = parseInt($(tds[7]).text());
        scrapedData7.push(name);
        //console.log(scrapedData2[0]);


    });


}

//Парсер кол-во очков
async function main7() {
    const result = await request.get("https://www.sports.ru/rfpl/table/");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = parseInt($(tds[8]).text());
        scrapedData8.push(name);
        //console.log(scrapedData2[0]);


    });


}


async function main8() {
    const result = await request.get("https://www.sports.ru/rfpl/bombardiers/?&s=goals&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr > td:nth-child(2) > div").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData9.push(name);
    });

    for (let i = 0; i < 5; i++) {
        let query = "UPDATE rpl.gl SET Namea = '" + scrapedData9[i] + "'  WHERE id = " + (i + 1) + ";";
        conn.query(query, (err, result) => {});
    }

}

async function main9() {
    const result = await request.get("https://www.sports.ru/rfpl/bombardiers/?&s=goals&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr > td.name-td.alLeft.bordR > div ").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData10.push(name);
    });



}

async function main10() {
    const result = await request.get("https://www.sports.ru/rfpl/bombardiers/?&s=goals&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = $(tds[4]).text();
        scrapedData11.push(name);
    });


}


async function main11() {
    const result = await request.get("https://www.sports.ru/rfpl/bombardiers/?&s=goal_passes&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr > td:nth-child(2) > div").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData12.push(name);
    });


}

async function main12() {
    const result = await request.get("https://www.sports.ru/rfpl/bombardiers/?&s=goal_passes&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr > td.name-td.alLeft.bordR > div ").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData13.push(name);
    });


}

async function main13() {
    const result = await request.get("https://www.sports.ru/rfpl/bombardiers/?&s=goal_passes&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = $(tds[6]).text();
        scrapedData14.push(name);
    });



}


async function main14() {
    const result = await request.get("https://www.sports.ru/rfpl/fouls/?&s=yellow_cards&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr > td:nth-child(2) > div").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData15.push(name);
    });



}

async function main15() {
    const result = await request.get("https://www.sports.ru/rfpl/fouls/?&s=yellow_cards&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr > td.name-td.alLeft.bordR > div ").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData16.push(name);
    });



}

async function main16() {
    const result = await request.get("https://www.sports.ru/rfpl/fouls/?&s=yellow_cards&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = $(tds[4]).text();
        scrapedData17.push(name);
    });


}


async function main17() {
    const result = await request.get("https://www.sports.ru/rfpl/fouls/?&s=red_cards&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr > td:nth-child(2) > div").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData18.push(name);
    });



}

async function main18() {
    const result = await request.get("https://www.sports.ru/rfpl/fouls/?&s=red_cards&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr > td.name-td.alLeft.bordR > div ").each((index, elemet) => {

        const tds = $(elemet).find("a");
        const name = $(tds[0]).text();
        scrapedData19.push(name);
    });



}

async function main19() {
    const result = await request.get("https://www.sports.ru/rfpl/fouls/?&s=red_cards&d=1&season=8071");
    const $ = cheerio.load(result);

    $("#branding-layout > div.pageLayout > div.contentLayout.js-active > div:nth-child(4) > div.layout-columns.layout-columns_nopaddingTop > div.mainPart.columns-layout__main.js-active > div.stat.mB6 > table > tbody > tr").each((index, elemet) => {

        const tds = $(elemet).find("td");
        const name = $(tds[5]).text();
        scrapedData20.push(name);
    });


}


// ГЛАВНАЯ ТАБЛИЦА
Zapusk();
setTimeout(OK, 2000);

function OK() {
    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.rp;")
        .then(result2 => {
            if (result2[0][0].zheka == 0) {
                for (let i = 0; i < 16; i++) {
                    // console.log('zapis');
                    var sql = "INSERT INTO rpl.rp (id, Name, Mat, Win , Draw , Loose, Scored, Conceded , Point ) VALUES ('" + (i + 1) + "','" + scrapedData1[i] + "','" + scrapedData2[i] + "','" + scrapedData3[i] + "','" + scrapedData4[i] + "','" + scrapedData5[i] + "','" + scrapedData6[i] + "','" + scrapedData7[i] + "','" + scrapedData8[i] + "')";
                    conn.query(sql, function(err, result) {
                        if (err) throw err;
                        //console.log("1 record inserted");
                    });
                }
            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}
setInterval(Zapusk, 30000);
setInterval(Upda, 32000);
//ТАБЛИЦА ГОЛОВ
Zapusk1();
setTimeout(OK1, 2000);

function OK1() {
    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.gl;")
        .then(result2 => {
            if (result2[0][0].zheka == 0) {
                for (let i = 0; i < 5; i++) {
                    // console.log('zapis');
                    var sql = "INSERT INTO rpl.gl (id, Namea, Club, Goal ) VALUES ('" + (i + 1) + "','" + scrapedData9[i] + "','" + scrapedData10[i] + "','" + scrapedData11[i] + "')";
                    conn.query(sql, function(err, result) {
                        if (err) throw err;
                        //console.log("1 record inserted");
                    });
                }
            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}
setInterval(Zapusk1, 30000);
setInterval(Upda1, 32000);
//ТАБЛИЦА АССИСТОВ
Zapusk2();
setTimeout(OK2, 2000);

function OK2() {
    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.ass;")
        .then(result2 => {
            if (result2[0][0].zheka == 0) {
                for (let i = 0; i < 5; i++) {
                    // console.log('zapis');
                    var sql = "INSERT INTO rpl.ass (id, Nameb, Logob, Assists ) VALUES ('" + (i + 1) + "','" + scrapedData12[i] + "','" + scrapedData13[i] + "','" + scrapedData14[i] + "')";
                    conn.query(sql, function(err, result) {
                        if (err) throw err;
                        //console.log("1 record inserted");
                    });
                }
            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}
setInterval(Zapusk2, 30000);
setInterval(Upda2, 32000);
//ТАБЛИЦА ЖЕЛТЫХ
Zapusk3();
setTimeout(OK3, 2000);

function OK3() {
    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.yell;")
        .then(result2 => {
            if (result2[0][0].zheka == 0) {
                for (let i = 0; i < 5; i++) {
                    // console.log('zapis');
                    var sql = "INSERT INTO rpl.yell (id, Namec, Logoc, Yellow ) VALUES ('" + (i + 1) + "','" + scrapedData15[i] + "','" + scrapedData16[i] + "','" + scrapedData17[i] + "')";
                    conn.query(sql, function(err, result) {
                        if (err) throw err;
                        //console.log("1 record inserted");
                    });
                }
            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}
setInterval(Zapusk3, 30000);
setInterval(Upda3, 32000);
//ТАБЛИЦА КРАСНЫХ
Zapusk4();
setTimeout(OK4, 2000);

function OK4() {
    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.red;")
        .then(result2 => {
            if (result2[0][0].zheka == 0) {
                for (let i = 0; i < 5; i++) {
                    // console.log('zapis');
                    var sql = "INSERT INTO rpl.red (id, Named, Logod, Red ) VALUES ('" + (i + 1) + "','" + scrapedData18[i] + "','" + scrapedData19[i] + "','" + scrapedData20[i] + "')";
                    conn.query(sql, function(err, result) {
                        if (err) throw err;
                        //console.log("1 record inserted");
                    });
                }
            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}
setInterval(Zapusk4, 30000);
setInterval(Upda4, 32000);
//ОСТАЛЬНОЕ
function Zapusk() {
    main();
    main1();
    main2();
    main3();
    main4();
    main5();
    main6();
    main7();
    // main8();
    // main9();
    // main10();
    // main11();
    // main12();
    // main13();
    // main14();
    // main15();
    // main16();
    // main17();
    // main18();
    // main19();
}

function Upda() {

    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.rp;")
        .then(result2 => {
            if (result2[0][0].zheka == 0)
                OK();
            else {


                for (let i = 0; i < 16; i++) {
                    let query = "UPDATE rpl.rp SET Name = '" + scrapedData1[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }
                for (let j = 0; j < 16; j++) {
                    let query1 = "UPDATE rpl.rp SET Mat = '" + scrapedData2[j] + "'  WHERE id = " + (j + 1) + ";";
                    conn.query(query1, (err, result) => {});
                    //console.log(scrapedData2[j]);
                }
                for (let j = 0; j < 16; j++) {
                    let query2 = "UPDATE rpl.rp SET Win = '" + scrapedData3[j] + "'  WHERE id = " + (j + 1) + ";";
                    conn.query(query2, (err, result) => {});
                    //console.log(scrapedData3[j]);
                }
                for (let j = 0; j < 16; j++) {
                    let query2 = "UPDATE rpl.rp SET Draw = '" + scrapedData4[j] + "'  WHERE id = " + (j + 1) + ";";
                    conn.query(query2, (err, result) => {});
                    //console.log(scrapedData3[j]);
                }
                for (let j = 0; j < 16; j++) {
                    let query2 = "UPDATE rpl.rp SET Loose = '" + scrapedData5[j] + "'  WHERE id = " + (j + 1) + ";";
                    conn.query(query2, (err, result) => {});
                    //console.log(scrapedData3[j]);
                }
                for (let j = 0; j < 16; j++) {
                    let query2 = "UPDATE rpl.rp SET Scored = '" + scrapedData6[j] + "'  WHERE id = " + (j + 1) + ";";
                    conn.query(query2, (err, result) => {});
                    //console.log(scrapedData3[j]);
                }
                for (let j = 0; j < 16; j++) {
                    let query2 = "UPDATE rpl.rp SET Conceded = '" + scrapedData7[j] + "'  WHERE id = " + (j + 1) + ";";
                    conn.query(query2, (err, result) => {});
                    //console.log(scrapedData3[j]);
                }
                for (let j = 0; j < 16; j++) {
                    let query2 = "UPDATE rpl.rp SET Point = '" + scrapedData8[j] + "'  WHERE id = " + (j + 1) + ";";
                    conn.query(query2, (err, result) => {});
                    //console.log(scrapedData3[j]);
                }
            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}

function Zapusk1() {
    main8();
    main9();
    main10();
}

function Upda1() {

    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.gl;")
        .then(result2 => {
            if (result2[0][0].zheka == 0)
                OK1();
            else {
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.gl SET Namea = '" + scrapedData9[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.gl SET Club = '" + scrapedData10[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                    //console.log(scrapedData10[i]);
                }
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.gl SET Goal = '" + scrapedData11[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }

            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}

function Zapusk2() {
    main11();
    main12();
    main13();
}


function Upda2() {

    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.ass;")
        .then(result2 => {
            if (result2[0][0].zheka == 0)
                OK2();
            else {
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.ass SET Nameb = '" + scrapedData12[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.ass SET Logob = '" + scrapedData13[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.ass SET Assists = '" + scrapedData14[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }

            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}

function Zapusk3() {
    main14();
    main15();
    main16();
}

function Upda3() {

    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.yell;")
        .then(result2 => {
            if (result2[0][0].zheka == 0)
                OK3();
            else {
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.yell SET Namec = '" + scrapedData15[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.yell SET Logoc = '" + scrapedData16[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.yell SET Yellow = '" + scrapedData17[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }

            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}

function Zapusk4() {
    main17();
    main18();
    main19();
}

function Upda4() {

    connection.promise().query("SELECT COUNT(*) AS zheka FROM rpl.red;")
        .then(result2 => {
            if (result2[0][0].zheka == 0)
                OK4();
            else {
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.red SET Named = '" + scrapedData18[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.red SET Logod = '" + scrapedData19[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }
                for (let i = 0; i < 5; i++) {
                    let query = "UPDATE rpl.red SET Red = '" + scrapedData20[i] + "'  WHERE id = " + (i + 1) + ";";
                    conn.query(query, (err, result) => {});
                }


            }
        }).catch(err => {
            console.log(err);
            res.end(JSON.stringify("ERROR"));
        })
}