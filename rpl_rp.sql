-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: rpl
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rp`
--

DROP TABLE IF EXISTS `rp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rp` (
  `id` int NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Mat` varchar(45) DEFAULT NULL,
  `Win` varchar(45) DEFAULT NULL,
  `Draw` varchar(45) DEFAULT NULL,
  `Loose` varchar(45) DEFAULT NULL,
  `Scored` varchar(45) DEFAULT NULL,
  `Conceded` varchar(45) DEFAULT NULL,
  `Point` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rp`
--

LOCK TABLES `rp` WRITE;
/*!40000 ALTER TABLE `rp` DISABLE KEYS */;
INSERT INTO `rp` VALUES (1,'Зенит','19','12','5','2','43','15','41'),(2,'ЦСКА','19','11','4','4','35','17','37'),(3,'Спартак','19','10','5','4','33','21','35'),(4,'Сочи','19','9','6','4','28','22','33'),(5,'Ростов','19','10','2','7','24','20','32'),(6,'Динамо','19','9','3','7','24','21','30'),(7,'Краснодар','19','9','3','7','35','19','30'),(8,'Локомотив','19','8','4','7','21','25','28'),(9,'Рубин','19','8','4','7','24','25','28'),(10,'Ахмат','19','7','5','7','19','21','26'),(11,'Химки','19','7','4','8','24','30','25'),(12,'Урал','19','4','9','6','16','23','21'),(13,'Ротор','19','3','5','11','9','30','14'),(14,'Арсенал Тула','19','3','5','11','17','30','14'),(15,'Уфа','19','3','4','12','14','31','13'),(16,'Тамбов','19','3','4','12','13','29','13');
/*!40000 ALTER TABLE `rp` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-30 23:29:12
