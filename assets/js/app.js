﻿function make(e) {
    if ((localStorage.getItem("realLogin") === null) || (localStorage.getItem("realLogin") === "")) {
        document.location.href = "login.html"
    } else {
        document.location.href = "cabin.html";
    }
}



$(function() {

    var header = $("#header"),
        introH = $("#intro").innerHeight(),
        scrollOffset = $(window).scrollTop();




    //Появление заголовка  
    checkScroll(scrollOffset);

    $(window).on("scroll", function() {

        scrollOffset = $(this).scrollTop();
        checkScroll(scrollOffset);


    });

    function checkScroll(scrollOffset) {

        if (scrollOffset >= introH) {
            header.addClass("fixed");
        } else {
            header.removeClass("fixed");
        }
    }


    //Плавный скролл

    $("[data-scroll]").on("click", function(event) {
        event.preventDefault();

        var $this = $(this),
            blockId = $this.data('scroll'),
            blockOffset = $(blockId).offset().top - $('header').innerHeight();

        $("#nav a").removeClass('active')
        $this.addClass('active');

        if ($("#nav_toggle").hasClass('active')) {
            $(window).on("scroll", function() {
                $('#nav_toggle').removeClass("active");
                $("#nav").removeClass("active");
            });
        }

        $('.section__title').removeClass('bg-highlight')
        $(blockId + ' .section__title').addClass('bg-highlight')

        $("html, body").animate({
            scrollTop: blockOffset
        }, 500);
    });


    //МЕНЮ

    $("#nav_toggle").on("click", function(event) {
        event.preventDefault();

        $(this).toggleClass("active");
        $("#nav").toggleClass("active");

    });


    //Работа аккардиона
    $("[data-collapse]").on("click", function(event) {
        event.preventDefault();


        var $this = $(this),
            blockId = $this.data('collapse');
        $this.toggleClass("active");

    });


});