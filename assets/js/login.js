const button = document.getElementById("button-login");
const goingLoginButton = document.getElementById("button-going-login")
const loginInput = document.getElementsByClassName("input-data")[0]
const pasInput = document.getElementsByClassName("input-data")[1]
const navt = document.getElementsByClassName("nav_toggle")
const nav = document.getElementsByClassName("nav")


document.getElementsByTagName("body")[0].scrollLeft = 0

let url = new URL(window.location.href)
const realUrl = url.protocol + "//" + url.host



for (let i = 0; i < 2; i++) {
    document.getElementsByClassName("input-data")[i].onchange = () => {
        if (document.getElementsByClassName("input-data")[i].value.length > 0)
            document.getElementsByTagName("label")[i].style.visibility = "visible"
        else
            document.getElementsByTagName("label")[i].style.visibility = "hidden"

        document.getElementsByClassName("input-data")[i].style.borderColor = "rgb(82, 82, 82)";
    }
}


pasInput.addEventListener('keydown', function(e) {
    if (e.keyCode == 13)
        loginButton.onclick()
})

function validateUsr(username) {
    let regexp = /[^A-z^А-я\d_]/;
    let forbiddenSymbols = username.match(regexp);
    if (forbiddenSymbols === null) {
        return false;
    } else return true;

}

function validatePass(username) {
    let regexp = /[^A-z^А-я\d\.\%\-\|\=\+\*\^\$\#\@\&]/;
    let forbiddenSymbols = username.match(regexp);
    if (forbiddenSymbols === null) {
        return false;
    } else return true;

}



async function newFetch(url, data) {
    res = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })

    //responce from server "resData"
    let resData
    if (res.ok) {
        resData = await res.json();
    } else {
        alert("Ошибка HTTP: " + res.status);
    }

    return resData
}


button.onclick = async function(event) {
    const data = {
        type: 0,
        login: loginInput.value,
        password: pasInput.value
    }
    let okay = true;
    for (let i = 0; i < 2; i++) {
        if (i === 1) {
            if (document.getElementsByClassName("input-data")[i].value.length < 4 || validatePass(document.getElementsByClassName("input-data")[i].value)) {
                document.getElementsByClassName("input-data")[i].style.borderColor = "red";
                document.getElementsByTagName("label")[i].style.visibility = "hidden";
                okay = false;
            }
        } else {
            if (document.getElementsByClassName("input-data")[i].value.length < 4 || validateUsr(document.getElementsByClassName("input-data")[i].value)) {
                document.getElementsByClassName("input-data")[i].style.borderColor = "red";
                okay = false;
                document.getElementsByTagName("label")[i].style.visibility = "hidden";
            }
        }
    }
    if (okay) {
        resData = await newFetch(realUrl, data)

        if (resData.code == "OK") {
            localStorage.setItem("curLogin", resData.id)
            localStorage.setItem("realLogin", resData.login)
            localStorage.setItem("realTeam", resData.teams)
            document.location.href = "cabin.html"
                // document.getElementById("username-profile").innerText = "resData.login"
        } else
            alert(resData)
    } else
        document.getElementsByClassName("info-post")[0].style.visibility = "visible";
}





//JQUERY
$(function() {

    var header = $("#header"),
        introH = $("#intro").innerHeight(),
        scrollOffset = $(window).scrollTop();




    //Появление заголовка  
    checkScroll(scrollOffset);

    $(window).on("scroll", function() {

        scrollOffset = $(this).scrollTop();
        checkScroll(scrollOffset);


    });

    function checkScroll(scrollOffset) {

        if (scrollOffset >= introH) {
            header.addClass("fixed");
        } else {
            header.removeClass("fixed");
        }
    }


    //Плавный скролл

    $("[data-scroll]").on("click", function(event) {
        event.preventDefault();

        var $this = $(this),
            blockId = $this.data('scroll'),
            blockOffset = $(blockId).offset().top - $('header').innerHeight();

        $("#nav a").removeClass('active')
        $this.addClass('active');

        if ($("#nav_toggle").hasClass('active')) {
            $(window).on("scroll", function() {
                $('#nav_toggle').removeClass("active");
                $("#nav").removeClass("active");
            });
        }

        $('.section__title').removeClass('bg-highlight')
        $(blockId + ' .section__title').addClass('bg-highlight')

        $("html, body").animate({
            scrollTop: blockOffset
        }, 500);
    });


    //МЕНЮ

    $("#nav_toggle").on("click", function(event) {
        event.preventDefault();

        $(this).toggleClass("active");
        $("#nav").toggleClass("active");

    });


    //Работа аккардиона
    $("[data-collapse]").on("click", function(event) {
        event.preventDefault();


        var $this = $(this),
            blockId = $this.data('collapse');
        $this.toggleClass("active");

    });


});