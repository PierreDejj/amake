const button = document.getElementById("button-login");
const goingLoginButton = document.getElementById("button-going-login")
const loginInput = document.getElementsByClassName("input-data")[0]
const pasInput = document.getElementsByClassName("input-data")[1]
const navt = document.getElementsByClassName("nav_toggle")
const nav = document.getElementsByClassName("nav")
const goingRegButton = document.getElementById("button-going-reg")
const registerButton = document.getElementById("button-register")


document.getElementsByTagName("body")[0].scrollLeft = 0

let url = new URL(window.location.href)
const realUrl = url.protocol + "//" + url.host

for (let i = 0; i < 4; i++) {
    document.getElementsByClassName("input-data")[i].onchange = () => {
        if (document.getElementsByClassName("input-data")[i].value.length > 0)
            document.getElementsByTagName("label")[i].style.visibility = "visible"
        else
            document.getElementsByTagName("label")[i].style.visibility = "hidden"

        document.getElementsByClassName("input-data")[i].style.borderColor = "rgb(82, 82, 82)";
    }
}

async function newFetch(url, data) {
    res = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })

    //responce from server "resData"
    let resData
    if (res.ok) {
        resData = await res.json();
    } else {
        alert("Ошибка HTTP: " + res.status);
    }

    return resData
}


function validateUsr(username) {
    let regexp = /[^A-z^А-я\d_]/;
    let forbiddenSymbols = username.match(regexp);
    if (forbiddenSymbols === null) {
        return false;
    } else return true;

}

function validatePass(username) {
    let regexp = /[^A-z^А-я\d\.\%\-\|\=\+\*\^\$\#\@\&]/;
    let forbiddenSymbols = username.match(regexp);
    if (forbiddenSymbols === null) {
        return false;
    } else return true;

}


registerButton.onclick = async() => {
    if (document.getElementsByClassName("input-data")[1].value != document.getElementsByClassName("input-data")[2].value)
        alert("Wrong confirm password")
    else {
        const data = {
            type: 1,
            login: document.getElementsByClassName("input-data")[0].value,
            password: document.getElementsByClassName("input-data")[1].value,
            teams: document.getElementsByClassName("input-data")[3].value,
            confirmpas: document.getElementsByClassName("input-data")[2].value
        }
        let okay = true;
        for (let i = 0; i < 3; i++) {
            if (i === 1 || i === 2) {
                if (document.getElementsByClassName("input-data")[i].value.length < 4 || validatePass(document.getElementsByClassName("input-data")[i].value)) {
                    document.getElementsByClassName("input-data")[i].style.borderColor = "red";
                    document.getElementsByTagName("label")[i].style.visibility = "hidden";
                    okay = false;
                }
            } else {
                if (document.getElementsByClassName("input-data")[i].value.length < 4 || validateUsr(document.getElementsByClassName("input-data")[i].value)) {
                    document.getElementsByClassName("input-data")[i].style.borderColor = "red";
                    document.getElementsByTagName("label")[i].style.visibility = "hidden";
                    okay = false;
                }
            }
        }
        if (okay) {
            resData = await newFetch(realUrl, data)

            if (resData > -1) {
                localStorage.setItem("curLogin", resData)
                localStorage.setItem("realLogin", data.login)
                localStorage.setItem("realTeam", data.teams)
                document.location.href = "cabin.html"
            } else
                alert(resData)
        } else
            document.getElementsByClassName("info-post")[0].style.visibility = "visible";

    }
}
document.getElementsByClassName("close-info-post")[0].onclick = () => {
    document.getElementsByClassName("info-post")[0].style.visibility = "hidden";
}


//JQUERY
$(function() {

    var header = $("#header"),
        introH = $("#intro").innerHeight(),
        scrollOffset = $(window).scrollTop();




    //Появление заголовка  
    checkScroll(scrollOffset);

    $(window).on("scroll", function() {

        scrollOffset = $(this).scrollTop();
        checkScroll(scrollOffset);


    });

    function checkScroll(scrollOffset) {

        if (scrollOffset >= introH) {
            header.addClass("fixed");
        } else {
            header.removeClass("fixed");
        }
    }


    //Плавный скролл

    $("[data-scroll]").on("click", function(event) {
        event.preventDefault();

        var $this = $(this),
            blockId = $this.data('scroll'),
            blockOffset = $(blockId).offset().top - $('header').innerHeight();

        $("#nav a").removeClass('active')
        $this.addClass('active');

        if ($("#nav_toggle").hasClass('active')) {
            $(window).on("scroll", function() {
                $('#nav_toggle').removeClass("active");
                $("#nav").removeClass("active");
            });
        }

        $('.section__title').removeClass('bg-highlight')
        $(blockId + ' .section__title').addClass('bg-highlight')

        $("html, body").animate({
            scrollTop: blockOffset
        }, 500);
    });


    //МЕНЮ

    $("#nav_toggle").on("click", function(event) {
        event.preventDefault();

        $(this).toggleClass("active");
        $("#nav").toggleClass("active");

    });


    //Работа аккардиона
    $("[data-collapse]").on("click", function(event) {
        event.preventDefault();


        var $this = $(this),
            blockId = $this.data('collapse');
        $this.toggleClass("active");

    });


});