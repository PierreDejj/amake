﻿function make(e) {
    localStorage.setItem("realLogin", "");
    localStorage.setItem("realTeam", "");
    localStorage.setItem("curLogin", "1");
    document.location.href = "index.html"
}




function loadStorage() {
    let body = {
        type: 8
    }

    const url = 'http://localhost:3000';

    fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            header: {
                'Content-Type': 'application/text'
            }
        }).then(response => response.json())
        .then(data8 => {
            localStorage.setItem("data8", JSON.stringify(data8));
        }).catch(console.error);
}


function tableHTML() {

    loadStorage();
    let object = JSON.parse(localStorage.getItem("data8"));
    let perevod = rus_to_latin2(localStorage.getItem("realTeam"));
    if (perevod.length > 10) {
        perevod = perevod.slice(0, 7);
    }

    let num = object.Name.indexOf(perevod);

    let code = '';

    code = code +
        "<tr>\n" +
        "    <td data-label=\"Клуб\">" + object.Name[num] + "</td>\n" +
        "    <td data-label=\"Забитые мячи\">" + object.NPGoalS[num] + "</td>\n" +
        "    <td data-label=\"Пропущенные мячи\">" + object.NPGoalC[num] + "</td>\n" +
        "    <td data-label=\"xG\" class=\"tr1\">" + object.xG[num] + "</td>\n" +
        "    <td data-label=\"xGa\" class=\"tr1\">" + object.xGa[num] + "</td>\n" +
        "    <td data-label=\"xG_diff\" class=\"tr2\">" + object.xG_diff[num] + "</td>\n" +
        "    <td data-label=\"xG_per_90\" class=\"tr3\">" + object.xG_per[num] + "</td>\n" +
        "    <td data-label=\"xGa_per_90\" class=\"tr3\">" + object.xGa_per[num] + "</td>\n" +
        "    <td data-label=\"Количество минут без голов\">" + object.Suho[num] + "</td>\n" +
        "    <td data-label=\"BigCh\" class=\"tr4\">" + object.BigCh[num] + "</td>\n" +
        "    <td data-label=\"BigChA\" class=\"tr4\">" + object.BigChA[num] + "</td>\n" +
        "    <td data-label=\"BigCh_diff\" class=\"tr4\">" + object.BigCh_diff[num] + "</td>\n" +
        "</tr>\n"

    document.write(code);
};


$(function() {

    var header = $("#header"),
        introH = $("#intro").innerHeight(),
        scrollOffset = $(window).scrollTop();




    //Появление заголовка  
    checkScroll(scrollOffset);

    $(window).on("scroll", function() {

        scrollOffset = $(this).scrollTop();
        checkScroll(scrollOffset);


    });

    function checkScroll(scrollOffset) {

        if (scrollOffset >= introH) {
            header.addClass("fixed");
        } else {
            header.removeClass("fixed");
        }
    }


    //Плавный скролл

    $("[data-scroll]").on("click", function(event) {
        event.preventDefault();

        var $this = $(this),
            blockId = $this.data('scroll'),
            blockOffset = $(blockId).offset().top - $('header').innerHeight();

        $("#nav a").removeClass('active')
        $this.addClass('active');

        if ($("#nav_toggle").hasClass('active')) {
            $(window).on("scroll", function() {
                $('#nav_toggle').removeClass("active");
                $("#nav").removeClass("active");
            });
        }

        $('.section__title').removeClass('bg-highlight')
        $(blockId + ' .section__title').addClass('bg-highlight')

        $("html, body").animate({
            scrollTop: blockOffset
        }, 500);
    });


    //МЕНЮ

    $("#nav_toggle").on("click", function(event) {
        event.preventDefault();

        $(this).toggleClass("active");
        $("#nav").toggleClass("active");

    });


    //Работа аккардиона
    $("[data-collapse]").on("click", function(event) {
        event.preventDefault();


        var $this = $(this),
            blockId = $this.data('collapse');
        $this.toggleClass("active");

    });


});


function rus_to_latin2(str) {

    const ru = new Map([
        ['а', 'a'],
        ['б', 'b'],
        ['в', 'v'],
        ['г', 'g'],
        ['д', 'd'],
        ['е', 'e'],
        ['є', 'e'],
        ['ё', 'e'],
        ['ж', 'j'],
        ['з', 'z'],
        ['и', 'i'],
        ['ї', 'yi'],
        ['й', 'i'],
        ['к', 'k'],
        ['л', 'l'],
        ['м', 'm'],
        ['н', 'n'],
        ['о', 'o'],
        ['п', 'p'],
        ['р', 'r'],
        ['с', 's'],
        ['т', 't'],
        ['у', 'u'],
        ['ф', 'f'],
        ['х', 'h'],
        ['ц', 'c'],
        ['ч', 'ch'],
        ['ш', 'sh'],
        ['щ', 'shch'],
        ['ы', 'y'],
        ['э', 'e'],
        ['ю', 'u'],
        ['я', 'ya'],
    ]);

    str = str.replace(/[ъь]+/g, '');

    return Array.from(str)
        .reduce((s, l) =>
            s + (
                ru.get(l) ||
                ru.get(l.toLowerCase()) === undefined && l ||
                ru.get(l.toLowerCase()).toUpperCase()
            ), '');
};

function getLogo1() {
    if (localStorage.getItem("realTeam") != null) {
        let perevod = rus_to_latin2(localStorage.getItem("realTeam"));
        if (perevod.length > 10) {
            perevod = perevod.slice(0, 7);
        }
        let code = "<img src=\"assets/images/cabinet/JPEG/" + perevod + "1.jpg\" alt=\"\"></img>"

        document.write(code);
    }
}

function getLogo2() {
    if (localStorage.getItem("realTeam") != null) {
        let perevod = rus_to_latin2(localStorage.getItem("realTeam"));
        if (perevod.length > 10) {
            perevod = perevod.slice(0, 7);
        }
        let code = "<img src=\"assets/images/cabinet/JPEG/" + perevod + "2.jpg\" alt=\"\"></img>"

        document.write(code);
    }
}


function getLogo3() {
    if (localStorage.getItem("realTeam") != null) {
        let perevod = rus_to_latin2(localStorage.getItem("realTeam"));
        if (perevod.length > 10) {
            perevod = perevod.slice(0, 7);
        }
        let code = "<img src=\"assets/images/cabinet/JPEG/" + perevod + "3.jpg\" alt=\"\"></img>"

        document.write(code);
    }
}