﻿function make(e) {
    if ((localStorage.getItem("realLogin") === "") || (localStorage.getItem("realLogin") === null)) {
        document.location.href = "login.html"
    } else {
        document.location.href = "cabin.html";
    }
}

var url = 'http://localhost:3000';


function loadStorage() {
    let body = {
        type: 3
    }



    fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            header: {
                'Content-Type': 'application/text'
            }
        }).then(response => response.json())
        .then(data => {
            //console.log(data);
            if ((data == "EROR BD") || (data == "EROR")) {
                alert(data);
                localStorage.removeItem("data");
            } else
                localStorage.setItem("data", JSON.stringify(data));
        }).catch(console.error);
}

function createHTML_for_Newest() {

    loadStorage();
    let object = JSON.parse(localStorage.getItem("data"));


    let code = '';


    for (let i = 0; i < object.Name.length; i++) {
        if (i < 1) {
            code = code +
                "<tr class=\"tr1 \">\n" +
                "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
                "        <td data-label=\" \"> <img class=\"table_img2 \" img allign=\"middle\" src=\"assets/images/table/" + object.Logo[i] + ".png\" alt=\" \"></td>\n" +
                "        <td data-label=\"Клуб \">" + object.Name[i] + "</td>\n" +
                "        <td data-label=\"Игры \">" + object.Mat[i] + "</td>\n" +
                "        <td data-label=\"Победы \">" + object.Win[i] + "</td>\n" +
                "        <td data-label=\"Ничьи \">" + object.Draw[i] + "</td>\n" +
                "        <td data-label=\"Поражения \">" + object.Loose[i] + "</td>\n" +
                "        <td data-label=\"Забитые мячи \">" + object.Scored[i] + "</td>\n" +
                "        <td data-label=\"Пропущенные мячи \">" + object.Conceded[i] + "</td>\n" +
                "        <td data-label=\"Разница мячей \">" + object.Razn[i] + "</td>\n" +
                "        <td data-label=\"Очки \">" + object.Point[i] + "</td>\n" +
                "</tr>\n"
        } else if ((i > 0) && (i < 2)) {
            code = code +
                "<tr class=\"tr2 \">\n" +
                "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
                "        <td data-label=\" \"> <img class=\"table_img2 \" img allign=\"middle\" src=\"assets/images/table/" + object.Logo[i] + ".png\" alt=\" \"></td>\n" +
                "        <td data-label=\"Клуб \">" + object.Name[i] + "</td>\n" +
                "        <td data-label=\"Игры \">" + object.Mat[i] + "</td>\n" +
                "        <td data-label=\"Победы \">" + object.Win[i] + "</td>\n" +
                "        <td data-label=\"Ничьи \">" + object.Draw[i] + "</td>\n" +
                "        <td data-label=\"Поражения \">" + object.Loose[i] + "</td>\n" +
                "        <td data-label=\"Забитые мячи \">" + object.Scored[i] + "</td>\n" +
                "        <td data-label=\"Пропущенные мячи \">" + object.Conceded[i] + "</td>\n" +
                "        <td data-label=\"Разница мячей \">" + object.Razn[i] + "</td>\n" +
                "        <td data-label=\"Очки \">" + object.Point[i] + "</td>\n" +
                "</tr>\n"
        } else if ((i > 11) && (i < 14)) {
            code = code +
                "<tr class=\"tr3 \">\n" +
                "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
                "        <td data-label=\" \"> <img class=\"table_img2 \" img allign=\"middle\" src=\"assets/images/table/" + object.Logo[i] + ".png\" alt=\" \"></td>\n" +
                "        <td data-label=\"Клуб \">" + object.Name[i] + "</td>\n" +
                "        <td data-label=\"Игры \">" + object.Mat[i] + "</td>\n" +
                "        <td data-label=\"Победы \">" + object.Win[i] + "</td>\n" +
                "        <td data-label=\"Ничьи \">" + object.Draw[i] + "</td>\n" +
                "        <td data-label=\"Поражения \">" + object.Loose[i] + "</td>\n" +
                "        <td data-label=\"Забитые мячи \">" + object.Scored[i] + "</td>\n" +
                "        <td data-label=\"Пропущенные мячи \">" + object.Conceded[i] + "</td>\n" +
                "        <td data-label=\"Разница мячей \">" + object.Razn[i] + "</td>\n" +
                "        <td data-label=\"Очки \">" + object.Point[i] + "</td>\n" +
                "</tr>\n"
        } else if ((i >= 14) && (i <= 16)) {
            code = code +
                "<tr class=\"tr4 \">\n" +
                "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
                "        <td data-label=\" \"> <img class=\"table_img2 \" img allign=\"middle\" src=\"assets/images/table/" + object.Logo[i] + ".png\" alt=\" \"></td>\n" +
                "        <td data-label=\"Клуб \">" + object.Name[i] + "</td>\n" +
                "        <td data-label=\"Игры \">" + object.Mat[i] + "</td>\n" +
                "        <td data-label=\"Победы \">" + object.Win[i] + "</td>\n" +
                "        <td data-label=\"Ничьи \">" + object.Draw[i] + "</td>\n" +
                "        <td data-label=\"Поражения \">" + object.Loose[i] + "</td>\n" +
                "        <td data-label=\"Забитые мячи \">" + object.Scored[i] + "</td>\n" +
                "        <td data-label=\"Пропущенные мячи \">" + object.Conceded[i] + "</td>\n" +
                "        <td data-label=\"Разница мячей \">" + object.Razn[i] + "</td>\n" +
                "        <td data-label=\"Очки \">" + object.Point[i] + "</td>\n" +
                "</tr>\n"
        } else {
            code = code +
                "<tr>\n" +
                "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
                "        <td data-label=\" \"> <img class=\"table_img2 \" img allign=\"middle\" src=\"assets/images/table/" + object.Logo[i] + ".png\" alt=\" \"></td>\n" +
                "        <td data-label=\"Клуб \">" + object.Name[i] + "</td>\n" +
                "        <td data-label=\"Игры \">" + object.Mat[i] + "</td>\n" +
                "        <td data-label=\"Победы \">" + object.Win[i] + "</td>\n" +
                "        <td data-label=\"Ничьи \">" + object.Draw[i] + "</td>\n" +
                "        <td data-label=\"Поражения \">" + object.Loose[i] + "</td>\n" +
                "        <td data-label=\"Забитые мячи \">" + object.Scored[i] + "</td>\n" +
                "        <td data-label=\"Пропущенные мячи \">" + object.Conceded[i] + "</td>\n" +
                "        <td data-label=\"Разница мячей \">" + object.Razn[i] + "</td>\n" +
                "        <td data-label=\"Очки \">" + object.Point[i] + "</td>\n" +
                "</tr>\n"

        }
    }
    document.write(code);

};


function loadStorage4() {
    let body = {
        type: 4
    }


    fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            header: {
                'Content-Type': 'application/text'
            }
        }).then(response => response.json())
        .then(data1 => {
            if ((data1 == "EROR BD") || (data1 == "EROR")) {
                alert(data1);
                localStorage.removeItem("data1");
            } else
                localStorage.setItem("data1", JSON.stringify(data1));
        }).catch(console.error);
}


function goalHTML() {

    loadStorage4();
    let object = JSON.parse(localStorage.getItem("data1"));


    let code = '';


    for (let i = 0; i < object.Name.length; i++) {
        code = code +
            "<tr>\n" +
            "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
            "        <td data-label=\"Игрок \">\n" +
            "           <div class=\"goal\"> " + object.Name[i] + " </div>\n" +
            "           <div class=\"goal2\">\n" +
            "               <span class=\"logos1\">\n" +
            "                   <img src=\"assets/images/table/" + object.Logo[i] + ".png\"  style=\"width: 25px;height: 16px; \" alt=\" \">\n" +
            "               </span>\n" +
            "               <span class=\"name1\">" + object.Club[i] + "</span>\n" +
            "           </div>\n" +
            "       </td>\n" +
            "       <td data-label=\"Голы \">" + object.Goal[i] + "</td>\n" +
            "</tr>\n"
    }
    document.write(code);
};



function loadStorage5() {
    let body = {
        type: 5
    }


    fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            header: {
                'Content-Type': 'application/text'
            }
        }).then(response => response.json())
        .then(data5 => {
            if ((data5 == "EROR BD") || (data5 == "EROR")) {
                alert(data5);
                localStorage.removeItem("data5");
            } else
                localStorage.setItem("data5", JSON.stringify(data5));
        }).catch(console.error);
}

function assHTML() {

    loadStorage5();
    let object = JSON.parse(localStorage.getItem("data5"));


    let code = '';


    for (let i = 0; i < object.Name.length; i++) {
        code = code +
            "<tr>\n" +
            "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
            "        <td data-label=\"Игрок \">\n" +
            "           <div class=\"goal\"> " + object.Name[i] + " </div>\n" +
            "           <div class=\"goal2\">\n" +
            "               <span class=\"logos1\">\n" +
            "                   <img src=\"assets/images/table/" + object.Logo[i] + ".png\"  style=\"width: 22px;height: 16px; \" alt=\" \">\n" +
            "               </span>\n" +
            "               <span class=\"name1\">" + object.Club[i] + "</span>\n" +
            "           </div>\n" +
            "       </td>\n" +
            "       <td data-label=\"Голевые пасы \">" + object.Assists[i] + "</td>\n" +
            "</tr>\n"
    }
    document.write(code);
};


function loadStorage6() {
    let body = {
        type: 6
    }


    fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            header: {
                'Content-Type': 'application/text'
            }
        }).then(response => response.json())
        .then(data6 => {
            if ((data6 == "EROR BD") || (data6 == "EROR")) {
                alert(data6);
                localStorage.removeItem("data6");
            } else
                localStorage.setItem("data6", JSON.stringify(data6));
        }).catch(console.error);
}

function yellHTML() {

    loadStorage6();
    let object = JSON.parse(localStorage.getItem("data6"));


    let code = '';


    for (let i = 0; i < object.Name.length; i++) {
        code = code +
            "<tr>\n" +
            "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
            "        <td data-label=\"Игрок \">\n" +
            "           <div class=\"goal\"> " + object.Name[i] + " </div>\n" +
            "           <div class=\"goal2\">\n" +
            "               <span class=\"logos1\">\n" +
            "                   <img src=\"assets/images/table/" + object.Logo[i] + ".png\"  style=\"width: 22px;height: 16px; \" alt=\" \">\n" +
            "               </span>\n" +
            "               <span class=\"name1\">" + object.Club[i] + "</span>\n" +
            "           </div>\n" +
            "       </td>\n" +
            "       <td data-label=\"Желтые карточки \">" + object.Yell[i] + "</td>\n" +
            "</tr>\n"
    }
    document.write(code);
};


function loadStorage7() {
    let body = {
        type: 7
    }


    fetch(url, {
            method: 'POST',
            body: JSON.stringify(body),
            header: {
                'Content-Type': 'application/text'
            }
        }).then(response => response.json())
        .then(data7 => {
            if ((data7 == "EROR BD") || (data7 == "EROR")) {
                alert(data7);
                localStorage.removeItem("data7");
            } else
                localStorage.setItem("data7", JSON.stringify(data7));
        }).catch(console.error);
}

function redHTML() {

    loadStorage7();
    let object = JSON.parse(localStorage.getItem("data7"));


    let code = '';


    for (let i = 0; i < object.Name.length; i++) {
        code = code +
            "<tr>\n" +
            "       <td data-label=\"Место \">" + [i + 1] + "</td>\n" +
            "        <td data-label=\"Игрок \">\n" +
            "           <div class=\"goal\"> " + object.Name[i] + " </div>\n" +
            "           <div class=\"goal2\">\n" +
            "               <span class=\"logos1\">\n" +
            "                   <img src=\"assets/images/table/" + object.Logo[i] + ".png\"  style=\"width: 22px;height: 16px; \" alt=\" \">\n" +
            "               </span>\n" +
            "               <span class=\"name1\">" + object.Club[i] + "</span>\n" +
            "           </div>\n" +
            "       </td>\n" +
            "       <td data-label=\"Красные карточки \">" + object.Red[i] + "</td>\n" +
            "</tr>\n"
    }
    document.write(code);
};

$(function() {

    var header = $("#header"),
        introH = $("#intro").innerHeight(),
        scrollOffset = $(window).scrollTop();




    //Появление заголовка  
    checkScroll(scrollOffset);

    $(window).on("scroll", function() {

        scrollOffset = $(this).scrollTop();
        checkScroll(scrollOffset);


    });

    function checkScroll(scrollOffset) {

        if (scrollOffset >= introH) {
            header.addClass("fixed");
        } else {
            header.removeClass("fixed");
        }
    }


    //Плавный скролл

    $("[data-scroll]").on("click", function(event) {
        event.preventDefault();

        var $this = $(this),
            blockId = $this.data('scroll'),
            blockOffset = $(blockId).offset().top - $('header').innerHeight();

        $("#nav a").removeClass('active')
        $this.addClass('active');

        if ($("#nav_toggle").hasClass('active')) {
            $(window).on("scroll", function() {
                $('#nav_toggle').removeClass("active");
                $("#nav").removeClass("active");
            });
        }

        $('.section__title').removeClass('bg-highlight')
        $(blockId + ' .section__title').addClass('bg-highlight')

        $("html, body").animate({
            scrollTop: blockOffset
        }, 500);
    });


    //МЕНЮ

    $("#nav_toggle").on("click", function(event) {
        event.preventDefault();

        $(this).toggleClass("active");
        $("#nav").toggleClass("active");

    });


    //Работа аккардиона
    $("[data-collapse]").on("click", function(event) {
        event.preventDefault();


        var $this = $(this),
            blockId = $this.data('collapse');
        $this.toggleClass("active");

    });


});