-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: rpl
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stats` (
  `id` int NOT NULL AUTO_INCREMENT,
  `NameClub` varchar(45) DEFAULT NULL,
  `NPGoals` varchar(45) DEFAULT NULL,
  `NPGoalsC` varchar(45) DEFAULT NULL,
  `xG` varchar(45) DEFAULT NULL,
  `xGa` varchar(45) DEFAULT NULL,
  `xG_diff` varchar(45) DEFAULT NULL,
  `xG_0_per_90` varchar(45) DEFAULT NULL,
  `xGa_0_per_90` varchar(45) DEFAULT NULL,
  `minutes_0` varchar(45) DEFAULT NULL,
  `BigCh` varchar(45) DEFAULT NULL,
  `BigChA` varchar(45) DEFAULT NULL,
  `BigCh_diff` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats`
--

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;
INSERT INTO `stats` VALUES (1,'Zenit','39','11','32,08','12,52','19,55','1,85','0,51','779','43','15','28'),(2,'Krasnodar','28','17','29,25','16,22','13,03','1,53','1,17','514','39','17','22'),(3,'Rubin','20','20','25,83','17,01','8,82','1,63','1,12','659','40','20','20'),(4,'CSKA','33','17','26,32','17,87','8,45','1,42','0,92','726','33','19','14'),(5,'Spartak','26','19','25,82','17,43','8,39','1,42','1,21','719','35','19','16'),(6,'Dinamo','19','15','20,24','17,54','2,7','1,45','1,02','668','19','24','-5'),(7,'Akhmat','14','18','18,59','16,22','2,38','1,1','0,96','750','21','21','0'),(8,'Sochi','24','20','18,64','17,04','1,6','1,23','1,04','743','25','20','5'),(9,'Rostov','19','18','17,51','16,2','1,3','1,14','0,89','694','26','18','8'),(10,'Lokomotiv','18','23','20,29','22,97','-2,68','1,45','1,33','606','25','34','-9'),(11,'Khimki','21','27','19,52','26,96','-7,44','1,12','1,3','697','27','39','-12'),(12,'Arsenal','16','24','15,29','22,87','-7,58','1,08','1,3','686','15','34','-19'),(13,'Ural','13','17','13,26','22,69','-9,44','0,76','1,55','730','13','27','-14'),(14,'Ufa','12','28','13,19','23,1','-9,91','0,57','1,39','790','13','32','-19'),(15,'Rotor','8','19','10,47','23,49','-13,02','0,6','1,68','696','14','28','-14'),(16,'Tambov','11','28','11,31','27,46','-16,15','0,62','1,43','761','14','35','-21');
/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-30 23:29:12
